import { createRouter,createWebHistory} from "vue-router";


// 路由信息
const routes = [

    {
        path: "/test",
        name: "test",
        component:()=>import("../views/testSocket.vue")
    },

    {
      path: "/",
      name: "home",
      component:()=>import("../layout/index.vue"),
      redirect: 'list',
      children:[
          {
              path:'/list',
              name:"list",
              component:()=>import("../views/user/userList.vue"),
              children:[
                  {
                      path:'/chat',
                      name:"chat",
                      component:()=>import("../views/chat/index.vue"),
                  },
              ]
          },
          {
              path:'/add',
              name:"add",
              component:()=>import("../views/user/userAdd.vue"),
          },
          {
              path:'/call',
              name:"call",
              component:()=>import("../views/conversation/index.vue"),

          }
      ]
    },



    {
        path: "/login",
        name: "login",
        component:  () => import('../views/login/index.vue'),
    },

    {
        path: "/register",
        name: "register",
        component:  () => import('../views/register/index.vue'),
    }
];





// 导出路由
const router = createRouter({
    history: createWebHistory(),
    routes,

});


router.beforeEach((to, from, next) => {
    let token = localStorage.getItem("fun-chat-token");  // 获取token
    if(to.name != 'login'&& to.name !== 'register'){  //如果页面需要判断是否有token
        if (!token){
            next({name:"login"});
        }else {
            next();
        }

    }else {
        next();
    }

})


export default router;
