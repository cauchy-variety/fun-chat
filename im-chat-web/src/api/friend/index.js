import request from '../../utils/request.js'


export function getfriendlist() {
    return request({
        url: '/friend/getAllFriends',
        method: 'get',
    })
}
