import request from '../../utils/request.js'

export function register(data) {
    return request({
        url: '/user/register',
        method: 'post',
        data:data
       
    })
}