import request from '../../utils/request.js'






export function addApply(data) {
    return request({
        url: '/friend/sendApplication',
        method: 'post',
        data:data
    })
}


export function getAllFriendsApply() {
    return request({
        url: '/friend/getFriendsApply',
        method: 'get',
    })
}

export function handleApply(data) {
    return request({
        url: '/friend/handleApply',
        method: 'post',
        data:data
    })
}
