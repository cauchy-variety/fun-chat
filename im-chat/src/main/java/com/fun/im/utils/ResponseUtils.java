package com.fun.im.utils;

import java.util.HashMap;
import java.util.Map;

public class ResponseUtils {

    public static Object ok() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("status", 0);
        obj.put("msg", "成功");
        return obj;
    }

    public static Object ok(Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("status", 0);
        obj.put("msg", "成功");
        obj.put("data", data);
        return obj;
    }

    public static Object ok(String msg, Object data) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("status", 0);
        obj.put("msg", msg);
        obj.put("data", data);
        return obj;
    }

    public static Object fail() {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("status", -1);
        obj.put("msg", "程序异常，请联系管理员处理");
        return obj;
    }

    public static Object fail(int status, String msg) {
        Map<String, Object> obj = new HashMap<String, Object>();
        obj.put("status", status);
        obj.put("msg", msg);
        return obj;
    }



    public static Object errApplyArgument() {
        return fail(410, "错误的参数");
    }

    public static Object inconsistentPassword() {
        return fail(411, "两次密码不匹配");
    }
    public static Object unExistent() {
        return fail(412, "用户不存在");
    }

    public static Object failRegistered() {
        return fail(413, "注册失败！请检查网络或联系网站管理员！");
    }

    public static Object failUpdatePassword() {
        return fail(414, "密码修改失败！请检查网络或联系网站管理员！");
    }

    public static Object failUsernameRepeat() {
        return fail(415, "用户名已存在");
    }



    public static Object failUsernameAndPassword() {
        return fail(416, "用户名或密码错误");
    }
    public static Object emptyValue() {
        return fail(417, "请输入内容");
    }
    public static Object captcha() {
        return fail(418, "验证码错误");
    }

    public static Object mismatchArgument() {
        return fail(419, "参数不匹配");
    }

    public static Object badArgument() {
        return fail(421, "参数不匹配");
    }
    public static Object noAddSameFriend () {
        return fail(420, "不可重复添加好友");
    }

    public static Object unlogin() {
        return fail(501, "登录过期！请重新登录！");
    }

    public static Object unauthz() {
        return fail(506, "无操作权限");
    }





/*    public static Object successUpdatePassword () {
        return ok("密码修改成功！");
    }
    public static Object successRegistered () {
        return ok("新用户注册成功！");
    }*/

}
