package com.fun.im.utils;




import com.alibaba.fastjson.JSON;
import com.fun.im.pojo.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expiration}")
    private long jwtExpiration;

    @Value("${jwt.tokenHeader}")
    private String jwtTokenHeader;

    /**
     * 用户登录成功后生成Jwt
     * 使用Hs256算法  私匙使用用户密码
     *
     * @return
     */
    public  String createJWT(User user) {
        Map<String, Object> map = new HashMap<>();
        map.put("user",user);
        return generatorToken(map);
    }

//    public String createJWT(User user) {
//        Map<String, Object> claims = new HashMap<>();
//        claims.put("userId", user.getId());
//        claims.put("username", user.getUsername());
//        return generatorToken(claims);
//        // 可以添加其他需要的非敏感信息
//    }

        private String generatorToken(Map<String, Object> map) {

        return Jwts.builder().setClaims(map)
                .setExpiration(generatorExpiration())
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public User getToken(String token){
        Claims body = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        LinkedHashMap user = (LinkedHashMap) body.get("user");
        return JSON.parseObject(JSON.toJSONString(user), User.class);
    }

    private Date generatorExpiration() {
        return new Date(System.currentTimeMillis() + jwtExpiration);
    }





}