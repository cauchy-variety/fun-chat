package com.fun.im.utils;

public class SnowflakeIdGenerator {

    // 私有静态实例，防止被引用，此处使用了懒汉式单例
    private static SnowflakeIdGenerator instance;

    // SnowflakeIdWorker实例
    private SnowflakeIdWorker idWorker;

    // 私有构造函数，防止被实例化
    private SnowflakeIdGenerator(long workerId, long datacenterId) {
        idWorker = new SnowflakeIdWorker(workerId, datacenterId);
    }

    // 获取单例实例
    public static synchronized SnowflakeIdGenerator getInstance(long workerId, long datacenterId) {
        if (instance == null) {
            instance = new SnowflakeIdGenerator(workerId, datacenterId);
        }
        return instance;
    }

    // 获取下一个ID
    public synchronized long nextId() {
        return idWorker.nextId();
    }

    // SnowflakeIdWorker类
    private static class SnowflakeIdWorker {
        private final long twepoch = 1672531200000L;

        // 机器id所占的位数
        private final long workerIdBits = 5L;

        // 数据中心id所占的位数
        private final long datacenterIdBits = 5L;

        // 最大机器ID
        private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

        // 最大数据中心ID
        private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

        // 序列号所占的位数
        private final long sequenceBits = 12L;

        // 机器ID向左移12位
        private final long workerIdShift = sequenceBits;

        // 数据中心ID向左移17位（5+12）
        private final long datacenterIdShift = sequenceBits + workerIdBits;

        // 时间截向左移22位（5+5+12）
        private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

        // 序列号掩码
        private final long sequenceMask = -1L ^ (-1L << sequenceBits);

        // 上一次生成ID的时间截
        private long lastTimestamp = -1L;

        // 序列号
        private long sequence = 0L;

        // 工作机器ID
        private final long workerId;

        // 数据中心ID
        private final long datacenterId;

        public SnowflakeIdWorker(long workerId, long datacenterId) {
            if (workerId > maxWorkerId || workerId < 0) {
                throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
            }
            if (datacenterId > maxDatacenterId || datacenterId < 0) {
                throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
            }
            this.workerId = workerId;
            this.datacenterId = datacenterId;
        }

        public synchronized long nextId() {
            long timestamp = timeGen();

            if (timestamp < lastTimestamp) {
                throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
            }

            if (lastTimestamp == timestamp) {
                sequence = (sequence + 1) & sequenceMask;
                if (sequence == 0) {
                    timestamp = tilNextMillis(lastTimestamp);
                }
            } else {
                sequence = 0L;
            }

            lastTimestamp = timestamp;

            return ((timestamp - twepoch) << timestampLeftShift) // 时间截位移
                    | (datacenterId << datacenterIdShift) // 数据中心ID位移
                    | (workerId << workerIdShift) // 机器ID位移
                    | sequence; // 序列号
        }

        protected long tilNextMillis(long lastTimestamp) {
            long timestamp = timeGen();
            while (timestamp <= lastTimestamp) {
                timestamp = timeGen();
            }
            return timestamp;
        }

        protected long timeGen() {
            return System.currentTimeMillis();

        }
    }


    /*public static void main(String[] args) {
        // 假设的工作机器ID和数据中心ID，实际使用时需要替换为真实的值
        long workerId = 1L;
        long datacenterId = 1L;

        // 获取工具类实例
        SnowflakeIdGenerator generator = SnowflakeIdGenerator.getInstance(workerId, datacenterId);

        // 生成User ID
        long userId = generator.nextId();

        // 打印生成的User ID
        System.out.println("Generated User ID: " + userId);
    }*/
}
