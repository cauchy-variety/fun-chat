package com.fun.im.controller;

import com.fun.im.annotaion.LoginUser;
import com.fun.im.pojo.Message;
import com.fun.im.pojo.User;
import com.fun.im.service.MessageService;
import com.fun.im.service.impl.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/list")
    public Object listMessage(@LoginUser User user,Long to,Integer pageNum,Integer pageSize){
        return messageService.listMessage(user.getId(), to, pageNum, pageSize);
    }

    @PostMapping("/add")
    public Object addMessage(@LoginUser User user, @RequestBody Message message){
        message.setFromId(user.getId());
        return messageService.addMessage(message);
    }

    @GetMapping("/listAll")
    public Object listAllMessage(@LoginUser User user,Long to){
        return messageService.listAllMessage(user.getId(), to);
    }

}
