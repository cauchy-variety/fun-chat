package com.fun.im.controller;

import com.fun.im.annotaion.LoginUser;
import com.fun.im.dto.RegistrationDto;
import com.fun.im.pojo.User;

import com.fun.im.service.UserService;
import com.fun.im.utils.JwtUtil;
import com.fun.im.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private UserService userService;

    @GetMapping
    public Object User(@LoginUser User user) {
        return ResponseUtils.ok(user);
    }

    @GetMapping("/getUserByUserName")
    public Object getUserByUserName(String username) {
        return ResponseUtils.ok(userService.findByUsername(username));
    }

/*    @PutMapping("/update-password/{id}")
    public Object updatePassword(Long id, @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword) {
        boolean success = userService.updatePassword(id, oldPassword, newPassword);
        if (success) {
            return ResponseUtils.ok();
        } else {
            return ResponseUtils.failUpdatePassword();
        }
    }*/

    @PostMapping("/register")
    public Object registerUser(@RequestBody  @Valid RegistrationDto registrationDto)  {
        System.out.println( "UserController "+registrationDto.toString());
       User user = userService.registerUser(registrationDto);
            return ResponseUtils.ok(jwtUtil.createJWT(user));

    }


}
