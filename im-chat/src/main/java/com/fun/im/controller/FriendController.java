package com.fun.im.controller;

import com.fun.im.annotaion.LoginUser;
import com.fun.im.pojo.Apply;
import com.fun.im.pojo.Friend;
import com.fun.im.pojo.User;
import com.fun.im.service.FriendService;
import com.fun.im.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/friend")
public class FriendController {

    @Autowired
    private FriendService friendService;

    //获取用户的全部好友
    @GetMapping("/getAllFriends")
    public Object getAllFriends(@LoginUser User user) {
        return friendService.UserFriendsList(user);
    }

    //获取用户有哪些朋友发来申请
    @GetMapping("/getFriendsApply")
    public Object getAllFriendsApply(@LoginUser User user) {
        return friendService.getApplyFriends(user.getId());
    }

    //请求添加对方为好友
    @PostMapping("/sendApplication")
    public Object toFriendSendApplication(@LoginUser User user, @RequestBody Apply apply) {

        apply.setUid(user.getId());
        System.out.println("FriendController  toFriendSendApplication"+ apply);
        return friendService.addApply(apply);

    }

    //处理好友申请
    @PostMapping("/handleApply")
    public Object HandleApply(@LoginUser User user, @RequestBody Apply apply ) {
        System.out.println("FriendController  HandleApply"+apply);
if(!friendService.HandleApply(user.getId(),apply.getTid(),apply.getIsAgree())){
    return ResponseUtils.fail();
}else {
    return ResponseUtils.ok();
}
    }




}
