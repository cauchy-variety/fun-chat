package com.fun.im.dao;

import com.fun.im.pojo.Friend;
import com.fun.im.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FriendDao {

    // 插入
    int insertFriend(Friend friend);



    // 查询
    Friend selectFriendById(@Param("id") Long id);
    List<Friend> selectAllFriends();

    // 更新
    int updateFriend(Friend friend);

    // 删除
    int deleteFriendById(@Param("id") Long id);

    /**
     * 检查两个用户是否已经是好友
     * @param userId 用户ID
     * @param friendId 好友ID
     * @return 如果已经是好友返回 true，否则返回 false
     */
    boolean checkIfFriends(Long userId, Long friendId);

    /**
     * 同意好友请求，在friend表中插入记录
     *
     * @param userId 用户ID
     * @param friendId 好友ID
     * @return 插入操作的结果
     */
    int agreeFriendRequest(Long userId, Long friendId);

    int agreeFriendRequestTwo(Long friendId, Long userId);

    /**
     * 查询指定用户ID的所有好友
     *
     * @param userId 用户ID
     * @return 好友列表
     */
    List<User> selectFriendsByUserId(Long userId);
}
