package com.fun.im.dao;

import com.fun.im.pojo.User;

import java.util.List;

public interface UserDao {
    int insertUser(User user);

    // 根据ID查询用户
    User selectUserById(Long id);

    // 更新用户信息
    int updateUser(User user);

    // 删除用户
    int deleteUserById(Long id);

    // 查询所有用户
    List<User> selectAllUsers();



    //登录和注册相关
    Integer isUsernameExist(String username);
    User findUserByUsername(String username);

    //
    int LogicalDelete(Long id);
    User findUserByUsernameAndPassword(String username, String password);


}
