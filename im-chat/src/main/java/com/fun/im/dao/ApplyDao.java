package com.fun.im.dao;



import com.fun.im.pojo.Apply;
import com.fun.im.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ApplyDao{

    // 插入
    int insertApply(Apply apply);

    // 更新
    int updateApply(Apply apply);

    // 根据ID删除
    int deleteApplyById(@Param("id") Long id);

    // 根据申请ID查询
    Apply selectApplyById(@Param("id") Long id);

    // 查询所有（或者根据需要添加条件）
    List<Apply> selectAllApplies();

    //根据userId查有哪些朋友申请
    List<User> findApply(@Param("uid")  Long uid);
    //通过用户id更新是否同意
    int updateIsAgree(@Param("uid") Long id, @Param("isAgree") Integer isAgree);

}
