package com.fun.im.dao;

import com.fun.im.pojo.Message;
import org.apache.ibatis.annotations.Param;


import java.util.List;
import java.util.Map;

public interface MessageDao {
    // 插入消息
    int insertMessage(Message message);

    // 根据ID删除消息
    int deleteMessageById(@Param("id") Long id);

    // 更新消息
    int updateMessage(Message message);

    // 根据ID查询消息
    Message selectMessageById(@Param("id") Long id);

    // 查询所有消息
    List<Message> selectAllMessages();

    //查询与好友的消息
    List<Map> selectMessagesWithFriend(Long myId, Long friendId);


    List<Map> selectAllMessagesWithFriend(Long myId, Long friendId);

}
