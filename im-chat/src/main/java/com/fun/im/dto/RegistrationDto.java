package com.fun.im.dto;
import javax.validation.constraints.*;
public class RegistrationDto {
        @NotBlank(message = "请输入邮箱号")
        @Email(message = "邮箱格式不正确")
        private String email;

        @NotBlank(message = "请输入用户名")
       @Pattern(regexp = "^[a-zA-Z0-9_]{4,20}$", message = "Username should be between 4 and 20 characters, containing only letters, numbers and underscores")
        private String username;

    @NotBlank(message = "请再次输入密码")
    @Size(min = 6, max = 50, message = "Password should be between 8 and 50 characters")
//    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$",
//            message = "Password should contain at least one digit, one uppercase, one lowercase letter, one special character and no whitespace")
    private String confirmPassword;

        @NotBlank(message = "请输入密码")
        @Size(min = 6, max = 50, message = "Password should be between 8 and 50 characters")
//        @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$",
//                message = "Password should contain at least one digit, one uppercase, one lowercase letter, one special character and no whitespace")
        private String password;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "RegistrationDto{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

