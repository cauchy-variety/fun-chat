package com.fun.im.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
public class SecurityConfig {


    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

/*    @Bean
    public WebSecurityConfigurerAdapter loginPageConfig(){
        return new WebSecurityConfigurerAdapter() {
            @Override
            public void configure(HttpSecurity httpSecurity) {
                httpSecurity.removeConfigurer(DefaultLoginPageConfigurer.class); //将默认加载的登录页配置删除
            }
        };
    }*/
}
