package com.fun.im.exception;



import com.fun.im.utils.*;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object exceptionHandler(HttpServletRequest req, Exception e) {
//        e.printStackTrace();
        if (e instanceof TokenException){
            return ResponseUtils.unlogin();
        }
        //token格式不对
        else if (e instanceof MalformedJwtException) {
            return ResponseUtils.unlogin();
        }
        //token过期
        else if (e instanceof ExpiredJwtException){
            return ResponseUtils.unlogin();
        }
        //用户名重复
        else if (e instanceof ErrArgumentException) {
            return ResponseUtils.badArgument();
        }
        //
        else if (e instanceof EmptyValueException) {
            return ResponseUtils.emptyValue();
        }
        //
        else if (e instanceof MismatchException) {
            return ResponseUtils.mismatchArgument();
        }


        else {
            return ResponseUtils.fail();
        }
    }

}