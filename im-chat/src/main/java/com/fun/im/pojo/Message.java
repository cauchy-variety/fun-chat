package com.fun.im.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    // 使用Long类型来存储bigInteger字段
    private Long id;

    // 谁发的
    private Long fromId;

    // 发给谁
    private Long toId;

    // 类型，使用Integer来存储tinyInteger
    private Integer type;

    // 具体内容
    private String content;
//   @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd-HH-mm")
    // 发送时间
    private Date createTime;


    public  Message (){

    }

    public Message(Long id, Long fromId, Long toId, Integer type, String content, Date createTime) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.type = type;
        this.content = content;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", fromId=" + fromId +
                ", toId=" + toId +
                ", type=" + type +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromId() {
        return fromId;
    }

    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    public Long getToId() {
        return toId;
    }

    public void setToId(Long toId) {
        this.toId = toId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
