package com.fun.im.pojo;
import java.io.Serializable;
import java.util.Date;

public class Friend implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; // 主键，自增
    private Long uid; // 用户ID
    private Long fid; // 好友ID
    private Date createTime; // 创建时间
    private Date updateTime; // 修改时间

    @Override
    public String toString() {
        return "Friend{" +
                "id=" + id +
                ", uid=" + uid +
                ", fid=" + fid +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    public Friend(Long id, Long uid, Long fid, Date createTime) {
        this.id = id;
        this.uid = uid;
        this.fid = fid;

        this.createTime = createTime;

    }

    public Friend() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}