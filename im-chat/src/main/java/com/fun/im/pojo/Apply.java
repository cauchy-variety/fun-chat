package com.fun.im.pojo;
import java.io.Serializable;
import java.util.Date;

public class Apply implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id; // 主键，自增
    private Long uid; // 用户ID
    private Long tid; // 好友ID（例如目标用户）
    private Integer isAgree; // 申请状态
    private String applyForMsg; // 申请信息
    private Date createTime; // 创建时间
    private Date updateTime; // 最终修改时间

    // 构造器
    public Apply() {
        // 默认构造器
    }

    public Apply(Long id, Long uid, Long tid, Integer isAgree, String applyForMsg, Date createTime) {
        this.id = id;
        this.uid = uid;
        this.tid = tid;
        this.isAgree = isAgree;
        this.applyForMsg = applyForMsg;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Apply{" +
                "id=" + id +
                ", uid=" + uid +
                ", tid=" + tid +
                ", isAgree=" + isAgree +
                ", applyForMsg='" + applyForMsg + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Integer getIsAgree() {
        return isAgree;
    }

    public void setIsAgree(Integer isAgree) {
        this.isAgree = isAgree;
    }

    public String getApplyForMsg() {
        return applyForMsg;
    }

    public void setApplyForMsg(String applyForMsg) {
        this.applyForMsg = applyForMsg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}



