package com.fun.im.service;

import com.fun.im.pojo.User;

import java.util.Map;

public interface LoginService {
    Object login(Map<String,String> map);
}
