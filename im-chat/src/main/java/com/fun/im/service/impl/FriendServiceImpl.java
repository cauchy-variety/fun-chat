package com.fun.im.service.impl;

import com.fun.im.dao.ApplyDao;
import com.fun.im.dao.FriendDao;
import com.fun.im.pojo.Apply;
import com.fun.im.pojo.User;
import com.fun.im.service.FriendService;
import com.fun.im.utils.ErrArgumentException;
import com.fun.im.utils.ResponseUtils;
import com.fun.im.utils.UsernameRepeatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendServiceImpl  implements FriendService {

    @Autowired
    private FriendDao friendDao;
    @Autowired
    private ApplyDao applyDao;


    public Object UserFriendsList(User user) {
       List<User> list =  friendDao.selectFriendsByUserId(user.getId());
        for (User user1 : list) {
            System.out.println("UserFriendsList"+user1);
        }
        return ResponseUtils.ok(list);
    }





    public Object addApply(Apply apply) {
        if (friendDao.checkIfFriends(apply.getUid(), apply.getTid())) {

            return ResponseUtils.noAddSameFriend();
    }

        applyDao.insertApply(apply);

        return ResponseUtils.ok();
    }

    public Object getApplyFriends(Long uid) {
        List<User> list = applyDao.findApply(uid);
        for (User user1 : list) {
            System.out.println( "FriendServiceImpl ：getApplyFriends "+user1);
        }
        return ResponseUtils.ok(list);

    }

    @Override
    public Boolean HandleApply(Long userId, Long friendId,Integer is_agree) {
        if (is_agree == null) {
            throw new ErrArgumentException();
        } else if (is_agree == 1) {
            System.out.println("is_agree");
            applyDao.updateIsAgree(userId, is_agree);
            friendDao.agreeFriendRequest(userId, friendId);
            friendDao.agreeFriendRequestTwo(friendId,userId);
            return true;
        }

        else {
            applyDao.updateIsAgree(userId, is_agree);
            return false;
        }



    }




}
