package com.fun.im.service;

import com.fun.im.pojo.Apply;
import com.fun.im.pojo.User;

public interface FriendService {

    Object UserFriendsList(User user);

    Object addApply(Apply apply);
    Object getApplyFriends(Long uid);

    Boolean HandleApply(Long userId,Long FriendId,Integer is_agree);




}
