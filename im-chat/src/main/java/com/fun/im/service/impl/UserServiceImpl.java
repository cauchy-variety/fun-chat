package com.fun.im.service.impl;

import com.fun.im.dao.UserDao;
import com.fun.im.dto.RegistrationDto;
import com.fun.im.pojo.User;
import com.fun.im.service.UserService;
import com.fun.im.utils.MismatchException;
import com.fun.im.utils.SnowflakeIdGenerator;
import com.fun.im.utils.UsernameRepeatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UserServiceImpl implements UserService {

    private static final long WORKER_ID = 1L;
    private static final long DATACENTER_ID = 1L;

    // 你可以将SnowflakeIdGenerator的实例作为一个bean来管理，但在这里我们简单地使用单例模式
    private SnowflakeIdGenerator snowflakeIdGenerator = SnowflakeIdGenerator.getInstance(WORKER_ID, DATACENTER_ID);
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    @Autowired
    public UserServiceImpl(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findUserByUsername(username);
    }

    @Override
    public User findById(Long id) {
        return userDao.selectUserById(id);
    }

    @Override
    public Boolean updateUser(User user) {

        int re = userDao.updateUser(user);
        if (re == 1) {
            return true;
        } else return false;
    }

    @Override
    public Boolean deleteUser(Long id) {
        int result = userDao.LogicalDelete(id);
        return result > 0;
    }

    @Override
    public User getUserDetailsById(Long id) {
        return userDao.selectUserById(id);
    }
    @Override
    public User registerUser(RegistrationDto registrationDto) {

        if (!registrationDto.getConfirmPassword().equals( registrationDto.getPassword())) {
            //两次密码不一致，则注册失败
            throw new MismatchException();
        }

        User existingUser = userDao.findUserByUsername(registrationDto.getUsername());
        if (existingUser != null) {
            // 如果用户名已存在，则注册失败
            throw new UsernameRepeatException();
        }

        // 加密新用户的密码
        String encryptedPassword = passwordEncoder.encode(registrationDto.getPassword());

        // 更新用户对象的密码为加密后的密码
        registrationDto.setPassword(encryptedPassword);
        //生成Id
        User user = new User();
        user.setId(snowflakeIdGenerator.nextId());
        user.setEmail(registrationDto.getEmail());
        user.setUsername(registrationDto.getUsername());
        user.setPassword(registrationDto.getPassword());
        //TODO
        /*后期应该优化的部分*/
        String[] strings = {"https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame.7p3fr4mx3f.webp",
                "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（35）.5fkf7n26mi.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（34）.6wqk9e6bd5.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（33）.5c0t9x93wo.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（32）.9nzmhgsfev.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（31）.86thfpoao6.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（30）.4g4bugzfgm.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（29）.6bgwn3bv2d.webp",
                "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（28）.9nzmhgsfes.webp",
                "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（27）.4g4bugzfgk.webp",
                "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（16）.5c0t9x93wb.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（15）.1sevk46e4a.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（13）.1lbnook8op.webp", "https://hellochina666.github.io/picx-images-hosting/fun-chat-user-image/Frame（14）.51dzgrtvqx.webp"};

        // 创建一个Random对象
        Random random = new Random();
        // 随机选择一个字符串
        String randomString = strings[random.nextInt(strings.length)];
        user.setImage(randomString);
        // 插入新用户到数据库

        int result = userDao.insertUser(user);
        // 根据插入结果返回注册是否成功
        if (result==0){
            //则注册失败
            throw new RuntimeException();
        }
        return user;
    }

}
