package com.fun.im.service;

import com.fun.im.pojo.Message;
import com.fun.im.pojo.User;

public interface MessageService {
   Object listMessage(Long userId, Long friendId, Integer pageNum, Integer pageSize);

    Object addMessage(Message message);

    Object listAllMessage(Long id, Long to);
}
