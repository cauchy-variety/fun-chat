package com.fun.im.service;

import com.fun.im.dto.RegistrationDto;
import com.fun.im.pojo.User;

public interface UserService {
    User findByUsername(String username);

    User findById(Long id);

    Boolean updateUser(User user);

    Boolean deleteUser(Long id);

    User getUserDetailsById(Long id);



    //返回一个只带id和username的对象
    User registerUser(RegistrationDto userRegistrationDto);


}
