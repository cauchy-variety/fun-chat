package com.fun.im.service.impl;

import com.fun.im.service.MessageService;
import com.fun.im.utils.EmptyValueException;
import com.fun.im.utils.UsernameRepeatException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.fun.im.dao.MessageDao;
import com.fun.im.pojo.Message;
import com.fun.im.pojo.User;
import com.fun.im.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    public Object listMessage(Long userId, Long friendId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Map> list = messageDao.selectMessagesWithFriend(userId, friendId);
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total", PageInfo.of(list).getTotal());
        return ResponseUtils.ok(map);
    }

    @Override
    public Object listAllMessage(Long id, Long to) {

        List<Map> list = messageDao.selectAllMessagesWithFriend(id, to);
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
         return ResponseUtils.ok(map);
    }

    public Object addMessage(Message message) {
       if(message.getContent()==null) {
            throw new EmptyValueException();
        }

        messageDao.insertMessage(message);
        return ResponseUtils.ok();
/*
Message newMessage =new Message();
        switch (message.getType()) {
            case 0:
                newMessage.setType(0);
                newMessage.setContent(message.getContent());
                break;
            case 1:
                newMessage.setType(1);
                break;
            case 2:
                newMessage.setType(2);
                break;
            default:
                newMessage.setType(3);
                break;*/

    }


}
