package com.fun.im.service.impl;

import com.fun.im.dao.UserDao;
import com.fun.im.pojo.User;
import com.fun.im.service.LoginService;
import com.fun.im.utils.JwtUtil;
import com.fun.im.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class LoginServiceImpl  implements LoginService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private PasswordEncoder passwordEncoder; // Spring Security中的密码加密工具


    public Object login(Map<String, String> map) {
        User user = userDao.findUserByUsername(map.get("username"));
        if (user == null){
            return ResponseUtils.unExistent();
        }

        if (!passwordEncoder.matches(map.get("password"), user.getPassword())){
            return ResponseUtils.failUsernameAndPassword();
        }

        return ResponseUtils.ok(jwtUtil.createJWT(user));
    }
}
